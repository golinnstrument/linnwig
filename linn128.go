package linnwig

import (
	"fmt"
	//"strings"
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	//"gitlab.com/goosc/bitwig"
	"gitlab.com/goosc/osc"
)

type linn128 struct {
	tr             Triggerer
	currentMode    Mode
	mixerMode      *MixerMode
	bitwigClipMode *BitwigClipMode
	settingsMode   *settingsSubmode
	isPlaying      bool
	sync.RWMutex
	closer chan bool
}

func (c *linn128) OnSlideTransition(row, col uint8) {
	if c.bitwigClipMode.inPlayingMode() {
		if col > 0 {
			c.bitwigClipMode.playSubmode.OnSlideTransition(row, col-1)
		}
	}
}

func (c *linn128) OnYData(row, col, data uint8) {
	if c.bitwigClipMode.inPlayingMode() {
		if col > 0 {
			c.bitwigClipMode.playSubmode.OnYData(row, col-1, data)
		}
	}
}

func (c *linn128) OnZData(row, col, data uint8) {
	if c.bitwigClipMode.inPlayingMode() {
		if col > 0 {
			c.bitwigClipMode.playSubmode.OnZData(row, col-1, data)
		}
	}
}

func (c *linn128) OnXDataLSB(row, col, lsb uint8) {
	if c.bitwigClipMode.inPlayingMode() {
		if col > 0 {
			c.bitwigClipMode.playSubmode.OnXDataLSB(row, col-1, lsb)
		}
	}
}

func (c *linn128) OnXDataMSB(row, col, msb uint8) {
	if c.bitwigClipMode.inPlayingMode() {
		if col > 0 {
			c.bitwigClipMode.playSubmode.OnXDataMSB(row, col-1, msb)
		}
	}
}

func (l *linn128) switchMode(m Mode) {
	if l.currentMode != nil {
		l.currentMode.Leave()
	}

	l.tr.SetColorAll(OFF)
	//m.SetTriggerer(l.tr)
	m.Enter()
	fmt.Printf("switching to mode %s\n", m.String())
	l.currentMode = m
}

func (l *linn128) setControl(x uint8, color linnstrument.Color) {
	l.tr.SetColor(x, 0, color)
}

func (l *linn128) setPad(x, y uint8, color linnstrument.Color) {
	l.tr.SetColor(x, y+1, color)
}

func (l *linn128) playPadPressed(x, y, velocity uint8) {
	if l.currentMode != nil {
		l.currentMode.PadPressed(x, y, velocity)
		return
	}
	fmt.Printf("unhandled pad pressed: %v/%v\n", x, y)
}

func (l *linn128) playPadReleased(x, y uint8) {
	if l.currentMode != nil {
		l.currentMode.PadReleased(x, y)
		return
	}
	fmt.Printf("unhandled pad released: %v/%v\n", x, y)
	l.setPad(x, y, OFF)
}

func (l *linn128) OnTouch(row, col, velocity uint8) {
	if velocity > 0 {
		l.eachPadPress(row, col, velocity)
		return
	}
	l.eachPadRelease(row, col)
}

func (l *linn128) eachPadPress(x, y, velocity uint8) {
	if y == 0 {
		l.controlPadPressed(x)
		return
	}
	l.playPadPressed(x, y-1, velocity)
}

func (l *linn128) controlPadPressed(x uint8) {
	switch x {
	case PerSplitSettings.Row():
		if l.currentMode == l.settingsMode {
			l.setControl(x, OFF)
			l.switchMode(l.bitwigClipMode)
		} else {
			l.setControl(x, PerSplitSettings.Color())
			l.switchMode(l.settingsMode)
		}
		//l.setControl(x, PerSplitSettings.Color())
	case Preset.Row():
		//l.setControl(x, Preset.Color())
	case Volume.Row():
		if l.currentMode == l.mixerMode {
			l.setControl(x, OFF)
			l.switchMode(l.bitwigClipMode)
		} else {
			l.setControl(x, Volume.Color())
			l.switchMode(l.mixerMode)
		}
	case Transpose.Row():
		color := l.currentMode.TransposePressed()
		l.setControl(Transpose.Row(), color)
	case Switch1.Row():
		color := l.currentMode.Switch1Pressed()
		l.setControl(Switch1.Row(), color)
	case Switch2.Row():
		l.doPlay()
	//l.setControl(x, Switch2.Color())
	case Split.Row():
		color := l.currentMode.SplitPressed()
		l.setControl(Split.Row(), color)
	}
}

func (l *linn128) controlPadReleased(x uint8) {
	//	fmt.Printf("control key released: %v\n", x)

	switch x {
	case PerSplitSettings.Row():
		if l.currentMode == l.settingsMode {
			l.setControl(x, PerSplitSettings.Color())
		} else {
			l.setControl(x, OFF)
		}
	case Switch1.Row():
		color := l.currentMode.Switch1Released()
		l.setControl(Switch1.Row(), color)
	case Transpose.Row():
		color := l.currentMode.TransposeReleased()
		l.setControl(Transpose.Row(), color)
	case Split.Row():
		color := l.currentMode.SplitReleased()
		l.setControl(Split.Row(), color)
	}

}

func (l *linn128) eachPadRelease(x, y uint8) {
	if y == 0 {
		l.controlPadReleased(x)
		return
	}
	l.playPadReleased(x, y-1)
}

func (l *linn128) OnMessage(path osc.Path, vals ...interface{}) {
	if path.HasPrefix("/time/str") || path.HasPrefix("/beat/str") || path.HasSuffix("/vu") {
		return
	}

	fmt.Printf("got message from bitwig: %q %v\n", path.String(), vals)
	/*
		if path == "/anysolo" {
			return
		}

		if path.HasPrefix("/fxparam") {
			return
		}

		if path.HasPrefix("/master") {
			return
		}

		if path.HasPrefix("/track/") {
			idx := strings.Index(path.String()[1:], "/")
			if idx < 0 {
				panic("must not happen")
			}
			l.mixerMode.EachMessage(osc.Path(path.String()[2+idx:]), vals...)
			return
		}

		switch path.String() {

		case string(reaper.STOP):
			//color := c.passiveControlColor(ctlPlay)
			playing := !reaper.HasAndIsOn(vals)
			//if playing {
			//color = c.activeControlColor(ctlPlay)
			//}
			l.Lock()
			l.isPlaying = playing
			l.Unlock()
			//c.Set(ctlPlay, 15, color)
			//		a.Switch(7, PLAY_BUTTON, playing)
		case string(reaper.PLAY):
			//color := c.passiveControlColor(ctlPlay)
			playing := reaper.HasAndIsOn(vals)
			//if playing {
			//color = c.activeControlColor(ctlPlay)
			//}
			l.Lock()
			l.isPlaying = playing
			l.Unlock()
		//c.Set(ctlPlay, 15, color)
		default:
			l.playtimeMode.EachMessage(path, vals...)
		}
	*/
}

func (l *linn128) Matches(path osc.Path) bool {
	/*
		if path == "/time" || path == "/time/str" || path == "/frames/str" || path == "/beat/str" || path == "/samples/str" || path == "/samples" {
			return false
		}
	*/
	return true
}

func (l *linn128) SetTriggerer(tr Triggerer) {
	l.tr = tr
}

func (l *linn128) doPlay() {
	//reaper.ACTION.Set(40044).WriteTo(l.tr)
}

func (p *linn128) blinkPlaying() chan bool {
	var isPlaying bool

	var closed = make(chan bool)

	go func() {
		defer func() {
			closed <- true
		}()
		for {
			select {
			case <-p.closer:
				fmt.Println("stop playing blinkerx")
				close(p.closer)
				return
			case <-time.After(time.Millisecond * 300):
				p.RLock()
				isPlaying = p.isPlaying
				p.RUnlock()

				if isPlaying {
					p.setControl(Switch2.Row(), OFF)
					time.Sleep(time.Millisecond * 300)
					p.setControl(Switch2.Row(), GREEN)
				}
			}
		}
	}()
	return closed
}

func (l *linn128) Init(closer chan bool) chan bool {
	l.setControl(6, OFF)
	l.tr.SetColorAll(OFF)

	l.mixerMode = newMixerMode(l.tr)
	l.bitwigClipMode = newBitwigClipMode(l.tr)
	l.settingsMode = newSettingsSubmode(l.tr, l.bitwigClipMode.playSubmode)

	l.setControl(Switch2.Row(), GREEN)

	//reaper.DEVICE_TRACK_COUNT.Set(32).WriteTo(l.tr)

	l.switchMode(l.mixerMode)
	l.setControl(Volume.Row(), Volume.Color())

	l.closer = closer
	return l.blinkPlaying()
}
