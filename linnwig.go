package linnwig

import (
	"gitlab.com/golinnstrument/linnstrument"
)

func New128(dev linnstrument.Device) Connection {
	return newConnection("linnwig128", dev, &linn128{})
}
