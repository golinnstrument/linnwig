package linnwig

import (
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
)

func newPlaySubMode(tr Triggerer, pt *BitwigClipMode) *playSubmode {
	m := &playSubmode{}
	m.tr = tr
	m.playtimeMode = pt
	m.midiOctave = 5
	m.calculateColorsAndKeys()
	m.lastLSB = -1
	for col, mp := range m.lastLSBs {
		for row := range mp {
			m.lastLSBs[col][row] = -1
		}
	}
	for row := range m.slideTransition {
		m.slideTransition[row] = -1
	}
	return m
}

// parsePitchWheelVals parses a 14-bit signed value, which becomes a signed int16.
//
// This is a slightly modified variant of the parsePitchWheelValue function
// from Joe Wass.
func parsePitchWheelVals(lsb byte, msb byte) (relative int16, absolute uint16) {
	var val uint16

	val = uint16((msb)&0x7f) << 7
	val |= uint16(lsb) & 0x7f

	// Turn into a signed value relative to the centre.
	relative = int16(val) - 0x2000

	return relative, val
}

var rowOffset uint8 = 4

//var startNoteMIDI uint8 = 36 // C

var noteNames = map[uint8]string{
	0:  "C",
	1:  "C#",
	2:  "D",
	3:  "D#",
	4:  "E",
	5:  "F",
	6:  "F#",
	7:  "G",
	8:  "G#",
	9:  "A",
	10: "A#",
	11: "B",
}

var scaleNotes = map[uint8]bool{
	0:  true,
	2:  true,
	4:  true,
	5:  true,
	7:  true,
	9:  true,
	11: true,
}

func (c *playSubmode) String() string {
	return "play submode"
}

var baseNote uint8 = 0

type playSubmode struct {
	padKeys   [14][7]int8
	padColors [14][7]uint8 // 0 = off, 1 = note, 2 = base note
	tr        Triggerer
	sync.RWMutex
	midiChannel       uint8 // 0-15
	midiOctave        uint8 // 0-10  5 = MIDI note 36
	playtimeMode      *BitwigClipMode
	fullVelocity      bool
	timbreYActive     bool
	timbreYCC74       bool // false = cc1, true = cc74
	loudnessZActive   bool
	loudnessZCC       uint8 // 0=poly pressure,1=channel pressure, 2=cc11
	pitchXActive      bool
	lastLSB           int8        // a cache for the last LSB: <0 == not set
	lastLSBs          [14][7]int8 // a cache for the last LSBs per note: <0 == not set
	rowPitchRef       [7]uint16   // a cache for the starting pitch of a row, is unset on each noteoff
	slideTransition   [7]int8     // set the slide transition mode; <0 == not set
	runningNotesOnRow [7]uint8
	bendRange         uint8 // 0=+-2, 1=+-3, 2 =+-12, 3=+-24
}

func (c *playSubmode) getTimbreY() (cc uint8, active bool) {
	c.RLock()
	active = c.timbreYActive
	cc = 1
	if c.timbreYCC74 {
		cc = 74
	}
	c.RUnlock()
	return
}

func (c *playSubmode) setTimbreY(cc uint8, active bool) {
	c.Lock()
	c.timbreYActive = active
	c.timbreYCC74 = false
	if cc == 74 {
		c.timbreYCC74 = true
	}
	c.Unlock()
	return
}

func (c *playSubmode) getLoudnessZ() (mode uint8, active bool) {
	c.RLock()
	active = c.loudnessZActive
	mode = c.loudnessZCC
	c.RUnlock()
	return
}

func (c *playSubmode) setLoudnessZ(mode uint8, active bool) {
	c.Lock()
	c.loudnessZActive = active
	c.loudnessZCC = mode
	c.Unlock()
	return
}

func (p *playSubmode) foundationNote() int8 {
	p.RLock()
	nt := int8(-24) + int8(p.midiOctave*12)
	p.RUnlock()
	return nt
}

var _ SubMode = &playSubmode{}

func (p *playSubmode) setPitchXData(on bool) {
	for row := uint8(0); row < 7; row++ {
		func(r uint8) {
			time.Sleep(time.Millisecond * time.Duration(r))
			p.tr.SetXData(r, on)
			time.Sleep(time.Microsecond * 200)
			p.tr.SetSlide(r, on)
		}(row)
	}

	/*
		if on {
			p.tr.SetDataDecimation(16)
		}
	*/
}

func (p *playSubmode) setTrimbreYData(on bool) {
	for x := uint8(0); x < 7; x++ {
		func(_x uint8) {
			time.Sleep(time.Millisecond * time.Duration(_x))
			p.tr.SetYData(x, on)
		}(x)
	}
}

func (p *playSubmode) setLoudnessZData(on bool) {
	for x := uint8(0); x < 7; x++ {
		func(_x uint8) {
			time.Sleep(time.Millisecond * time.Duration(_x))
			p.tr.SetZData(x, on)
		}(x)
	}
}

func (c *playSubmode) paintKeys() {
	c.RLock()
	padColors := c.padColors
	c.RUnlock()
	for x := uint8(0); x < groupStopRow; x++ {
		for y := uint8(0); y < sceneLaunchCol; y++ {
			color := linnstrument.ColorOff

			switch padColors[y][x] {
			case 1:

				color = GREEN
			case 2:
				color = CYAN
			}

			c.Set(x, y, color)
		}
	}
}

func (c *playSubmode) calculateColorsAndKeys() {
	startNoteMIDI := c.foundationNote()

	padColors := c.padColors
	padKeys := c.padKeys
	var note uint8
	for x := uint8(0); x < groupStopRow; x++ {
		for y := uint8(0); y < sceneLaunchCol; y++ {
			note = ((y + 1 + ((7 - x) * rowOffset)) - 7)
			res := startNoteMIDI + int8(note)
			padKeys[y][x] = res
			var color uint8 = 0
			switch {
			case res < 0 || res > 127:
				color = 0
			case note%12 == baseNote:
				color = 2
			case scaleNotes[note%12]:
				color = 1
			}
			padColors[y][x] = color
		}
	}

	c.Lock()
	c.padColors = padColors
	c.padKeys = padKeys
	c.Unlock()
}

func (p *playSubmode) OnSlideTransition(row, col uint8) {
	if row > 6 {
		return
	}

	//	fmt.Printf("slide transition: col: %v row: %v\n", col, row)

	if col > 12 {
		p.noteOff(row, col, int8(col))
		//		p.Lock()
		//		p.slideTransition[row] = -1
		//		p.Unlock()
		return
	}
	p.Lock()
	p.slideTransition[row] = int8(col)
	p.Unlock()
	//	fmt.Printf("slide transistion row %v col %v\n", row, col)
}

func (c *playSubmode) OnYData(row, col, data uint8) {
	//	fmt.Printf("OnYData: row: %v col: %v\n", row, col)
	if col > 13 || row > 6 {
		return
	}
	cc, _ := c.getTimbreY()
	// global for now (without MPE)
	msg := channel.Channel(c.midiChannel).ControlChange(cc, data)
	c.tr.WriteMIDI(msg)
}

func (c *playSubmode) OnZData(row, col, data uint8) {
	//	fmt.Printf("onZData: row: %v col: %v\n", row, col)
	if col > 13 || row > 6 {
		return
	}
	mode, _ := c.getLoudnessZ()

	var msg midi.Message
	ch := channel.Channel(c.midiChannel)

	switch mode {
	case 0:
		c.RLock()
		key := c.padKeys[col][row]
		c.RUnlock()
		if key < 0 || key > 127 {
			return
		}
		msg = ch.PolyAftertouch(uint8(key), data)
	case 1:
		// global for now (without MPE)
		msg = ch.Aftertouch(data)
	case 2:
		// global for now (without MPE)
		msg = ch.ControlChange(11, data)
	default:
		return
	}
	c.tr.WriteMIDI(msg)
}

func (c *playSubmode) OnXDataLSB(row, col, lsb uint8) {
	if row > 6 || col > 13 {
		return
	}
	c.Lock()
	c.lastLSB = int8(lsb)
	c.lastLSBs[col][row] = int8(lsb)
	c.Unlock()
	//	fmt.Printf("got LSB for row %v col %v data %v\n", row, col, lsb)
}

func (c *playSubmode) OnXDataMSB(row, col, msb uint8) {
	if row > 6 || col > 13 {
		return
	}
	c.Lock()
	lastLSB := c.lastLSB
	lastLSBForKey := c.lastLSBs[col][row]
	c.lastLSB = -1
	c.lastLSBs[col][row] = -1
	c.Unlock()

	_ = lastLSBForKey

	// here we only deal with global pitchbend (for MPE we would use lastLSBForKey)
	if lastLSB < 0 {
		// calculate pitchbend from msb?
	} else {

		// calculate pitchbend from lsb and msb
		_, abs := parsePitchWheelVals(uint8(lastLSB), msb)

		c.RLock()
		refPitch := c.rowPitchRef[row]
		bendRange := c.bendRange
		c.RUnlock()

		if refPitch == 0 {
			refPitch = abs
			c.Lock()
			c.rowPitchRef[row] = refPitch
			c.Unlock()
		}

		val := int16(abs) - int16(refPitch)

		factor := int16(24) // 2 tones

		switch bendRange {
		case 0:
			factor = 24 // 2 halftones
		case 1:
			factor = 12 // 3 halftones
		case 2:
			factor = 4 // 12 halftones
		case 3:
			factor = 2 // 24 halftones
		}

		msg := channel.Channel(c.midiChannel).Pitchbend(val * factor)
		c.tr.WriteMIDI(msg)
		//		fmt.Printf("pitchbend row %v col %v lsb: %v msb: %v pitchbend %v (abs: %v, ref: %v)\n", row, col, lastLSB, msb, val, abs, refPitch)
	}
	//	fmt.Printf("got MSB for row %v col %v data %v\n", row, col, msb)
}

func (c *playSubmode) Enter() {
	c.playtimeMode.paintControls()
	c.paintKeys()
}

func (c *playSubmode) Leave() {
}

func (t *playSubmode) Set(x, y uint8, color linnstrument.Color) {
	t.tr.SetColor(x, y+1, color)
}

func (c *playSubmode) PadPressed(x, y, velocity uint8) {
	//	fmt.Printf("pad pressed: x: %v y: %v\n", x, y)
	c.RLock()
	key := c.padKeys[y][x]
	fullvelocity := c.fullVelocity
	slideTransition := c.slideTransition[x]
	c.RUnlock()

	if key < 0 || key > 127 || slideTransition >= 0 {
		return
	}

	//	fmt.Printf("note on: %v (x: %v, y: %v)\n", key, x, y)

	if fullvelocity {
		velocity = 127
	}
	msg := channel.Channel(c.midiChannel).NoteOn(uint8(key), velocity)
	err := c.tr.WriteMIDI(msg)
	_ = err
	/*
		if err != nil {
			fmt.Errorf("could not send MIDI message %v: %v\n", msg, err)
		}
	*/
	c.Lock()
	c.runningNotesOnRow[x] = y
	c.Unlock()
	c.Set(x, y, RED)
	//fmt.Printf("note on: %v (%s) with velocity %v\n", key, noteNames[key%12], velocity)
}

func (c *playSubmode) noteOff(x, y uint8, slideTransition int8) {
	c.Lock()
	c.rowPitchRef[x] = 0
	c.slideTransition[x] = -1
	running := c.runningNotesOnRow[x]
	c.runningNotesOnRow[x] = 0
	if slideTransition >= 0 && running > 0 {
		y = running
	}
	key := c.padKeys[y][x]
	colorNo := c.padColors[y][x]
	//	fmt.Printf("unset slide transition and ref pitch row: %v col: %v\n", x, y)
	c.Unlock()

	if key < 0 || key > 127 {
		return
	}

	//	fmt.Printf("note off: %v\n", key)
	msg := channel.Channel(c.midiChannel).NoteOff(uint8(key))
	err := c.tr.WriteMIDI(msg)
	_ = err
	/*
		if err != nil {
			fmt.Errorf("could not send MIDI message %v: %v\n", msg, err)
		}
	*/

	color := OFF
	switch colorNo {
	case 1:
		color = GREEN
	case 2:
		color = CYAN
	}

	c.Set(x, y, color)

	//	fmt.Printf("note off: %v (%s)\n", key, noteNames[key%12])
}

func (c *playSubmode) PadReleased(x, y uint8) {
	c.RLock()
	slideTransition := c.slideTransition[x]
	c.RUnlock()

	if slideTransition == int8(y) {
		return
	}

	c.noteOff(x, y, slideTransition)
}
