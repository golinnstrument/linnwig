package linnwig

import (
	"fmt"

	"gitlab.com/golinnstrument/linnstrument"
)

var _ SubMode = &transposeSubmode{}

func newTransposeSubmode(tr Triggerer, pm *playSubmode) *transposeSubmode {
	m := &transposeSubmode{}
	m.tr = tr
	m.playSubmode = pm
	return m
}

type transposeSubmode struct {
	tr          Triggerer
	playSubmode *playSubmode
}

func (c *transposeSubmode) String() string {
	return "transpose submode"
}

func (c *transposeSubmode) clearAll() {
	for x := uint8(0); x < c.tr.Rows(); x++ {
		for y := uint8(0); y < c.tr.Cols(); y++ {
			c.tr.SetColor(x, y, OFF)
		}
	}
}

func (*transposeSubmode) SplitPressed() linnstrument.Color {
	return OFF
}

func (*transposeSubmode) Switch1Pressed() linnstrument.Color {
	return OFF
}

func (*transposeSubmode) Switch1Released() linnstrument.Color {
	return OFF
}

func (*transposeSubmode) TransposePressed() linnstrument.Color {
	return OFF
}

func (*transposeSubmode) TransposeReleased() linnstrument.Color {
	return OFF
}

func (*transposeSubmode) SplitReleased() linnstrument.Color {
	return OFF
}

func (s *transposeSubmode) Enter() {
	s.clearAll()
	s.paintOctave()
}

func (s *transposeSubmode) paintOctave() {
	// vals 0-10, 5 is at col7 all row 3

	oct := s.getOctave()
	octCol := 2 + oct

	for y := uint8(2); y < 13; y++ {
		color := OFF
		switch {
		case y == 7:
			color = CYAN
		case octCol == y:
			color = GREEN
		case octCol < y && y < 7:
			color = GREEN
		case y < octCol && y > 7:
			color = GREEN
		}
		s.Set(3, y, color)
	}

	s.Set(3, octCol, GREEN)
	s.Set(3, 7, CYAN)
}

func (*transposeSubmode) Leave() {

}

func (t *transposeSubmode) Set(x, y uint8, color linnstrument.Color) {
	t.tr.SetColor(x, y+1, color)
}

func (c *transposeSubmode) PadPressed(x, y, velocity uint8) {
	switch {
	case x == 3 && y > 1 && y < 13:
		c.setOctave(y - 2)
		c.paintOctave()
	default:
		fmt.Printf("transpose key %v/%v\n", x, y)
	}
}

func (c *transposeSubmode) getOctave() (ch uint8) {
	c.playSubmode.RLock()
	ch = c.playSubmode.midiOctave
	c.playSubmode.RUnlock()
	return
}

func (c *transposeSubmode) setOctave(oct uint8) {
	fmt.Printf("oct set to %v\n", oct)
	c.playSubmode.Lock()
	c.playSubmode.midiOctave = oct
	c.playSubmode.Unlock()
	c.playSubmode.calculateColorsAndKeys()
}

func (c *transposeSubmode) PadReleased(x, y uint8) {
}
