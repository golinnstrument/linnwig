# linnwig

Control Bitwig Studio from the LinnStrument

## requirements

### LinnStrument

- you get it from https://www.rogerlinndesign.com/linnstrument
- for now only 128 pad version is supported
- update the firmware (OS) to version 2.2.1 or higher

### Bitwig Studio

- you get it from https://bitwig.com
- only version 3.2.5 or higher is supported

### DrivenByMoss

- you get it from http://www.mossgrabers.de/Software/Bitwig/Bitwig.html
- take version 12.2.1
- read there how to install it

### midicat

- you can download it from https://github.com/gomidi/midicat/releases/download/v0.3.6/midicat-binaries.zip (Windows and Linux)
- for MacOsX you need to build it with Go (go install gitlab.com/gomidi/midicat) 

### linnwig

- for now you need Go to install it (go install gitlab.com/golinnstrument/linnwig/cmd/linnwig)
- make sure the binary `linnwig` or `linnwig.exe` is in the PATH

### add a virtual MIDI port

- depends on your operating system:
  - linux: use snd_virmidi (alsa)
  - windows: use https://www.tobias-erichsen.de/software/loopmidi.html
  - MacOsX: ???

### Bitwig configuration

- start Bitwig Studio
- add an OSC controller
- configure it to receive from Port 8000 and send to Port 9000 
- make sure that Bitwig can see the virtual MIDI port
- disable any device inside Bitwig that tries to connect to the LinnStrument
- create a track listening on the virtual MIDI port, arm it

### run linnwig

- open a shell (cmd.exe on windows)
- run `linnwig ports` to get a list of the available MIDI ports
- find the port number for the virtual MIDI in port
- run `linnwig [port]` where `[port]` is the port number of the virtual MIDI in port
- debug infos are shown in the shell
- to end your session, press CTRL+C
- run `linnwig help` for more options if needed

## Issues

- sometimes the LinnStrument does not switch properly into user firmware mode (It is properly switched when the lights
  are changing.) Reconnecting the LinnStrument and then restarting `linnwig` helps. 

## Documentation

- not there yet