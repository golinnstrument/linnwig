package linnwig

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	//"gitlab.com/goosc/bitwig"
	"gitlab.com/goosc/osc"
	//	"gitlab.com/goosc/reaper/reaperactions"
)

func newMixerMode(tr Triggerer) *MixerMode {
	m := &MixerMode{}
	m.tr = tr
	//m.action = reaperactions.New(tr)
	return m
}

type MixerMode struct {
	tr             Triggerer
	trackMuted     [32]bool
	trackRecArmed  [32]bool
	trackSoloed    [32]bool
	trackLatched   [32]bool
	trackMonitored [32]bool
	trackVol       [32]float32
	bank2          bool
	sync.RWMutex
	isActiveMode bool
	//action       *reaperactions.Action
}

func (t *MixerMode) String() string {
	return "mixer mode"
}

const (
	trackArmRow       uint8 = 0
	trackSoloRow      uint8 = 1
	trackMuteRow      uint8 = 2
	trackVolUpRow     uint8 = 3
	trackVolDownRow   uint8 = 4
	trackMonitorRow   uint8 = 5
	trackAutoLatchRow uint8 = 6
	SceneRow          uint8 = 7
)

// returns the track for the given y, taking into account the active bank
func (m *MixerMode) getTrack(y uint8) uint8 {
	if y > 15 {
		return 0
	}
	m.RLock()
	bank2 := m.bank2
	m.RUnlock()
	if bank2 {
		return y + 16
	}
	return y
}

func (m *MixerMode) soloTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	wasSoloed := m.trackSoloed[track]
	m.trackSoloed[track] = !wasSoloed
	m.Unlock()
	//	fmt.Printf("soloing track: %v %v\n", track, !wasSoloed)
	//reaper.TRACK_SOLO.Set(track+1).WriteTo(m.tr, !wasSoloed)
}

func (m *MixerMode) recArmTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	wasArmed := m.trackRecArmed[track]
	m.trackRecArmed[track] = !wasArmed
	m.Unlock()
	//	fmt.Printf("recArming track: %v %v\n", track, !wasArmed)
	//reaper.TRACK_REC_ARM.Set(track+1).WriteTo(m.tr, !wasArmed)
}

func (m *MixerMode) muteTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	wasMuted := m.trackMuted[track]
	m.trackMuted[track] = !wasMuted
	m.Unlock()
	//	fmt.Printf("muting track: %v %v\n", track, !wasMuted)
	//reaper.TRACK_MUTE.Set(track+1).WriteTo(m.tr, !wasMuted)
}

/*
func (m *TrackMode) selectTrack(y uint8) {
	track := m.getTrack(y)
	ReaperUnselectAllTracks(m.tr)
	reaper.TRACK_SELECT.Set(track+1).WriteTo(m.tr, true)
}
*/

func (m *MixerMode) monitorTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	wasMonitored := m.trackMonitored[track]
	m.trackMonitored[track] = !wasMonitored
	m.Unlock()
	//reaper.TRACK_MONITOR.Set(track+1).WriteTo(m.tr, !wasMonitored)
}

func (m *MixerMode) latchAutoTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	wasLatched := m.trackLatched[track]
	m.trackLatched[track] = !wasLatched
	m.Unlock()
	//reaperactions.UnselectAllTracks(m.tr)
	time.Sleep(time.Millisecond * 5)
	//reaper.TRACK_SELECT.Set(track+1).WriteTo(m.tr, true)
	time.Sleep(time.Millisecond * 5)
	/*
		if !wasLatched {
			reaperactions.AutoLatchSelectedTrack(m.tr)
		} else {
			reaperactions.ReadAutoTrack(m.tr)
		}
	*/
}

func (m *MixerMode) volUpTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	m.trackVol[track] += 2
	if m.trackVol[track] > 0 {
		m.trackVol[track] = 0
	}
	vol := m.trackVol[track]
	m.Unlock()
	_ = vol
	//	fmt.Printf("vol up track: %v\n", track)
	//reaper.TRACK_VOLUME_f.Set(track+1).WriteTo(m.tr, vol)
}

func (m *MixerMode) volDownTrack(y uint8) {
	if y > 15 {
		return
	}
	track := m.getTrack(y)
	m.Lock()
	m.trackVol[track] -= 2
	if m.trackVol[track] < -40 {
		m.trackVol[track] = -40
	}
	vol := m.trackVol[track]
	m.Unlock()
	_ = vol
	//	fmt.Printf("vol down track: %v\n", track)
	//reaper.TRACK_VOLUME_f.Set(track+1).WriteTo(m.tr, vol)
}

func (m *MixerMode) jumpToMarker(marker uint8) {
	if marker > 16 {
		return
	}
	//	fmt.Printf("jump to marker: %v\n", marker)
	//reaper.GOTO_MARKER.Set(marker).WriteTo(m.tr)
}

func (t *MixerMode) TransposePressed() linnstrument.Color {
	t.Lock()
	oldBank2 := t.bank2
	t.bank2 = !oldBank2
	t.Unlock()
	t.refreshState()

	if !oldBank2 {
		return PINK
	}
	return OFF
}

func (t *MixerMode) refreshState() {
	color := OFF
	t.RLock()
	if t.bank2 {
		color = PINK
	}

	t.tr.SetColor(Transpose.Row(), 0, color)

	for y := uint8(0); y < 16; y++ {
		idx := y
		if t.bank2 {
			idx += 16
		}

		if t.trackRecArmed[idx] {
			t.Set(trackArmRow, y, t.rowActiveColor(trackArmRow))
		} else {
			t.Set(trackArmRow, y, t.rowPassiveColor(trackArmRow))
		}

		if t.trackSoloed[idx] {
			t.Set(trackSoloRow, y, t.rowActiveColor(trackSoloRow))
		} else {
			t.Set(trackSoloRow, y, t.rowPassiveColor(trackSoloRow))
		}

		if t.trackLatched[idx] {
			t.Set(trackAutoLatchRow, y, t.rowActiveColor(trackAutoLatchRow))
		} else {
			t.Set(trackAutoLatchRow, y, t.rowPassiveColor(trackAutoLatchRow))
		}

		if t.trackMuted[idx] {
			t.Set(trackMuteRow, y, t.rowActiveColor(trackMuteRow))
		} else {
			t.Set(trackMuteRow, y, t.rowPassiveColor(trackMuteRow))
		}

		if t.trackMonitored[idx] {
			t.Set(trackMonitorRow, y, t.rowActiveColor(trackMonitorRow))
		} else {
			t.Set(trackMonitorRow, y, t.rowPassiveColor(trackMonitorRow))
		}

	}
	t.RUnlock()
}

func (t *MixerMode) rowActiveColor(row uint8) linnstrument.Color {
	switch row {
	case trackArmRow: // arm
		return RED
	case trackSoloRow: // solo
		return ORANGE
	case trackMuteRow: // mute
		return BLUE
	case trackVolUpRow: //
		return GREEN
	case trackVolDownRow: //
		return YELLOW
	case trackMonitorRow: //
		return WHITE
	case trackAutoLatchRow: //
		return MAGENTA
	case SceneRow: //
		return OFF
	}

	return OFF
}

func (t *MixerMode) rowPassiveColor(row uint8) linnstrument.Color {
	switch row {
	case trackArmRow: // arm
		return PINK
	case trackSoloRow: // solo
		return LIME
	case trackMuteRow: // mute
		return CYAN
	case trackVolUpRow: //
		return GREEN
	case trackVolDownRow: //
		return YELLOW
	case trackMonitorRow: //
		return OFF
	case trackAutoLatchRow: //
		return PINK
	case SceneRow: //
		return OFF
	}
	return OFF
}

func (t *MixerMode) Set(x, y uint8, color linnstrument.Color) {
	t.tr.SetColor(x, y+1, color)
}

func (t *MixerMode) Enter() {
	t.Lock()
	t.isActiveMode = true
	t.Unlock()
	for x := uint8(0); x < 8; x++ {
		rowColor := t.rowPassiveColor(x)
		for y := uint8(0); y < 16; y++ {
			t.Set(x, y, rowColor)
		}
	}
	t.refreshState()
}

func (a *MixerMode) EachMessage(path osc.Path, vals ...interface{}) {
	fmt.Printf("track message %q\n", path.String())
	/*
		if path == "solo/toggle" {
			return
		}

		if path == "solo" {
			return
		}

		if path == "volume" {
			return
		}

		if path == "vu" {
			return
		}

		idx := strings.LastIndex(path.String(), "/")
		if idx < 0 {
			fmt.Printf("reaper said: %s %v\n", path, vals)
			return
		}
		suffix := path.String()[idx:]

		switch suffix {
		case "/toggle":
		case "/vu":
		case "/L":
		case "/R":
		case "/select":
		case "/autotrim":
		case "/db": // ignore
		case "/volume": // ignore
		case "/monitor":
			var track int
			_, err := path.Scan("%d/monitor", &track)
			if err == nil {
				a.Lock()
				a.trackMonitored[track-1] = reaper.HasAndIsOn(vals)
				a.Unlock()
				if a.isActiveMode {
					a.refreshState()
				}
			}

		//	reaper said: /track/2/autolatch [1]
		case "/autolatch":
			var track int
			_, err := path.Scan("%d/autolatch", &track)
			if err == nil {
				//			fmt.Printf("mute track %v to %v\n", track, reaper.HasAndIsOn(vals))
				a.Lock()
				a.trackLatched[track-1] = reaper.HasAndIsOn(vals)
				a.Unlock()
				if a.isActiveMode {
					a.refreshState()
				}
				//a.refreshTrackInfos()
				//a.Unlock()
				//			a.Switch(2, uint8(reatrack-1), reaper.HasAndIsOn(vals))
			}
		case "/mute":
			var track int
			_, err := path.Scan("%d/mute", &track)
			if err == nil {
				//			fmt.Printf("mute track %v to %v\n", track, reaper.HasAndIsOn(vals))
				a.Lock()
				a.trackMuted[track-1] = reaper.HasAndIsOn(vals)
				a.Unlock()
				if a.isActiveMode {
					a.refreshState()
				}
				//			a.Switch(2, uint8(reatrack-1), reaper.HasAndIsOn(vals))
			}
		case "/solo":
			var track int
			_, err := path.Scan("%d/solo", &track)
			if err == nil {
				//track := a.getTrack(uint8(reatrack) - 1)
				//fmt.Printf("solo track %v to %v\n", track, reaper.HasAndIsOn(vals))
				a.Lock()
				a.trackSoloed[track-1] = reaper.HasAndIsOn(vals)
				a.Unlock()
				if a.isActiveMode {
					a.refreshState()
				}
				//a.Switch(0, uint8(reatrack-1), reaper.HasAndIsOn(vals))
			}
		case "/recarm":
			var track int
			_, err := path.Scan("%d/recarm", &track)
			if err == nil {
				//track := a.getTrack(uint8(reatrack) - 1)
				//			fmt.Printf("recarm track %v to %v\n", track, reaper.HasAndIsOn(vals))
				a.Lock()
				a.trackRecArmed[track-1] = reaper.HasAndIsOn(vals)
				a.Unlock()
				if a.isActiveMode {
					a.refreshState()
				}
				//			a.refreshTrackInfos()
				//			a.Unlock()
				//a.Switch(1, uint8(reatrack-1), reaper.HasAndIsOn(vals))
			}
		default:
			fmt.Printf("reaper said: %s %v\n", path, vals)
		}
	*/

	//	a.Flush()
}

func (t *MixerMode) Leave() {
	t.Lock()
	t.isActiveMode = false
	t.Unlock()
}

func (t *MixerMode) PadPressed(x, y, velocity uint8) {
	idx := y
	t.RLock()
	bank2 := t.bank2
	t.RUnlock()
	if bank2 {
		idx += 16
	}
	switch x {
	case trackArmRow: // arm
		t.recArmTrack(y)
		//		t.trackRecArmed[idx] = !t.trackRecArmed[idx]
	case trackSoloRow: // solo
		t.soloTrack(y)
		//		t.trackSoloed[idx] = !t.trackSoloed[idx]
	case trackMuteRow: // mute
		t.muteTrack(y)
		//		t.trackMuted[idx] = !t.trackMuted[idx]
	case trackVolUpRow: //
		t.Set(x, y, RED)
		t.volUpTrack(y)
	case trackVolDownRow: //
		t.Set(x, y, RED)
		t.volDownTrack(y)
	case trackMonitorRow: //
		//		t.Set(x, y, linnstrument.Red)
		//		t.selectTrack(y)
		t.monitorTrack(y)
	case trackAutoLatchRow: //
		t.latchAutoTrack(y)
		//		t.trackLatched[idx] = !t.trackLatched[idx]
	case SceneRow: //
		t.Set(x, y, RED)
		t.jumpToMarker(y + 1)
	}

	t.refreshState()
}

func (t *MixerMode) Switch1Pressed() linnstrument.Color {
	//t.action.ToggleMixerVisible()
	return ORANGE
}

func (t *MixerMode) Switch1Released() linnstrument.Color {
	return OFF
}

func (t *MixerMode) TransposeReleased() linnstrument.Color {
	t.RLock()
	bank2 := t.bank2
	t.RUnlock()

	if bank2 {
		return PINK
	}
	return OFF
}

func (c *MixerMode) SplitPressed() linnstrument.Color {
	return OFF
}
func (c *MixerMode) SplitReleased() linnstrument.Color {
	return OFF
}

func (t *MixerMode) PadReleased(x, y uint8) {
	switch x {
	case trackVolUpRow, trackVolDownRow, SceneRow: //
		t.Set(x, y, t.rowActiveColor(x))
	}
}

func (t *MixerMode) SetTriggerer(tr Triggerer) {
	t.Lock()
	t.tr = tr
	t.Unlock()
}

var _ Mode = &MixerMode{}
