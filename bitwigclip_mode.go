package linnwig

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/goosc/bitwig"
	"gitlab.com/goosc/osc"
)

type SubMode interface {
	String() string
	Enter()
	Leave()
	PadPressed(x, y, velocity uint8)
	PadReleased(x, y uint8)
}

func newBitwigClipMode(tr Triggerer) *BitwigClipMode {
	m := &BitwigClipMode{}
	m.Triggerer = tr
	//m.action = reaperactions.New(tr)
	m.clipSubmode = newClipSubmode(tr, m)
	m.playSubmode = newPlaySubMode(tr, m)
	m.transposeSubmode = newTransposeSubmode(tr, m.playSubmode)
	return m
}

type BitwigClipMode struct {
	clipSubmode *clipSubmode
	//	settingsSubmode *settingsSubmode
	playSubmode      *playSubmode
	transposeSubmode *transposeSubmode

	activeSubMode SubMode
	//action        *reaperactions.Action

	sync.RWMutex
	inTriggerMode  bool
	clipCutterMode uint8 // 0 = off, 1 = start position, 2 = length

	inDeleteMode bool
	Triggerer

	metronomeOn bool
	overdubOn   bool

	isActive bool
}

const (
	colorActiveTrackClip     linnstrument.Color = BLUE
	colorActiveTrackSelected linnstrument.Color = CYAN

	colorFilledClip         linnstrument.Color = LIME
	colorFilledSelectedClip linnstrument.Color = GREEN

	colorEmptyClipSelected     linnstrument.Color = ORANGE
	colorClipRecording         linnstrument.Color = RED
	colorClipRecordingSelected linnstrument.Color = MAGENTA

	colorStopGroup     linnstrument.Color = WHITE
	colorTrackRunning  linnstrument.Color = BLUE
	colorSceneLauncher linnstrument.Color = YELLOW
)

/*
const keyPattern3 = ``
const keyPattern4 = ``
const keyPattern5 = ``
const keyPattern6 = ``
const keyPattern7 = ``
const keyPattern8 = ``
const keyPatternNoOverlap = ``
const keyPatternGuitar = ``
*/

const runningTrackRow uint8 = 7

func (c *BitwigClipMode) setRunningTrack(y uint8) {
	if y > 13 {
		return
	}
	c.Set(runningTrackRow, y, colorTrackRunning)
}

func (c *BitwigClipMode) unsetRunningTrack(y uint8) {
	c.Set(runningTrackRow, y, colorStopGroup)
}

var _ Mode = &BitwigClipMode{}

func (c *BitwigClipMode) Switch1Pressed() linnstrument.Color {
	//c.action.ShowPlaytimeToggle()
	return CYAN
}

func (c *BitwigClipMode) Switch1Released() linnstrument.Color {
	return OFF
}

func (c *BitwigClipMode) TransposeReleased() linnstrument.Color {
	if c.activeSubMode == c.transposeSubmode {
		return PINK
	}

	return OFF
}

func (c *BitwigClipMode) switchSubMode(submode SubMode) {
	c.Lock()
	oldSubmode := c.activeSubMode
	c.activeSubMode = submode
	c.Unlock()

	if oldSubmode != nil && oldSubmode != submode {
		fmt.Printf("leaving submode %s\n", oldSubmode.String())
		oldSubmode.Leave()
	}
	fmt.Printf("entering submode %s\n", submode.String())
	submode.Enter()
}

func (c *BitwigClipMode) SplitPressed() linnstrument.Color {
	c.Lock()
	activeSubmode := c.activeSubMode
	c.Unlock()

	_, inPlayMode := activeSubmode.(*playSubmode)

	if inPlayMode {
		c.switchSubMode(c.clipSubmode)
		return OFF
	}

	c.switchSubMode(c.playSubmode)
	return BLUE
}
func (c *BitwigClipMode) SplitReleased() linnstrument.Color {
	c.Lock()
	activeSubmode := c.activeSubMode
	c.Unlock()

	_, inPlayMode := activeSubmode.(*playSubmode)

	if inPlayMode {
		return BLUE
	}

	return OFF
}

const clipControlCol uint8 = 15

func (c *BitwigClipMode) inPlayingMode() bool {
	c.RLock()
	isActive := c.isActive
	isPlaying := c.activeSubMode == c.playSubmode
	c.RUnlock()
	return isActive && isPlaying
}

func (c *BitwigClipMode) EachMessage(path osc.Path, vals ...interface{}) {
	/*
		if path.HasPrefix("/tempo") {
			return
		}

		if path.HasPrefix("/fx/") {
			return
		}
	*/
	switch path.String() {
	case string(bitwig.Click):
		//case string(reaper.METRONOME):
		//isOn := reaper.HasAndIsOn(vals)
		color := passiveControlColor(ctlClick)
		/*
			if isOn {
				color = activeControlColor(ctlClick)
			}
		*/
		c.Lock()
		//c.metronomeOn = isOn
		c.Unlock()
		c.Set(ctlClick, clipControlCol, color)
	default:
		fmt.Printf("bitwig said: %s %v\n", path, vals)
	}

}

func (c *BitwigClipMode) Enter() {
	c.Lock()
	c.isActive = true
	c.Unlock()

	_, isSettings := c.activeSubMode.(*settingsSubmode)

	if isSettings || c.activeSubMode == nil {
		c.switchSubMode(c.clipSubmode)
	} else {
		c.activeSubMode.Enter()
	}
}

const groupStopRow = 7
const sceneLaunchCol = 14

func (c *BitwigClipMode) paintControls() {
	c.Triggerer.SetColor(Transpose.Row(), 0, OFF)
	c.paintSceneLaunchers()
	c.paintToolbar()
	c.paintTrackStopper()
	c.Set(groupStopRow, sceneLaunchCol, ORANGE) // stop all button
}

func (c *BitwigClipMode) paintSceneLaunchers() {
	for x := uint8(0); x < groupStopRow; x++ {
		c.Set(x, sceneLaunchCol, colorSceneLauncher)
	}
}

func (c *BitwigClipMode) paintTrackStopper() {
	c.clipSubmode.RLock()
	active := c.clipSubmode.activeTrackClips
	c.clipSubmode.RUnlock()
	//	fmt.Printf("active track clips %#v\n", active)
	for y := uint8(0); y < sceneLaunchCol; y++ {
		color := colorStopGroup
		if active[y] >= 0 {
			color = colorTrackRunning
		}
		c.Set(groupStopRow, y, color)
	}
}

func (c *BitwigClipMode) Leave() {
	c.activeSubMode.Leave()
	c.Lock()
	c.isActive = false
	c.Unlock()
}

func (c *BitwigClipMode) PadPressed(x, y, velocity uint8) {
	switch {

	// clip actions
	case y == clipControlCol:
		c.controlPressed(x)

	// stop all clips
	case y == sceneLaunchCol && x == groupStopRow:
		c.RLock()
		clipCutterMode := c.clipCutterMode
		c.RUnlock()

		switch clipCutterMode {
		case 0:
			c.StopAllClips()
		case 1:
			//c.action.StopForSure()
			time.Sleep(time.Millisecond * 10)
			//c.clipSubmode.playtime.SelectClipStartDecrement()
			c.Set(groupStopRow, sceneLaunchCol, RED)
		case 2:
			//c.action.StopForSure()
			time.Sleep(time.Millisecond * 10)
			c.Set(groupStopRow, sceneLaunchCol, RED)
			//c.clipSubmode.playtime.SelectClipLengthDecrement()
		}

	// trigger scene
	case y == sceneLaunchCol:
		c.triggerScene(x)

	// stop track
	case x == groupStopRow:
		c.TrackStop(y)

	// we pressed a clip
	case x < groupStopRow && y < sceneLaunchCol:
		c.activeSubMode.PadPressed(x, y, velocity)
	default:
		panic(fmt.Sprintf("unhandled pad pressed: %v/%v\n", x, y))
	}
}

func (c *BitwigClipMode) PadReleased(x, y uint8) {
	if y == clipControlCol {
		c.controlReleased(x)
	}

	if y == sceneLaunchCol && x == groupStopRow {
		c.RLock()
		clipCutterMode := c.clipCutterMode
		c.RUnlock()
		if clipCutterMode > 0 {
			c.Set(groupStopRow, sceneLaunchCol, OFF)
		}
	}

	if x < groupStopRow && y < sceneLaunchCol {
		c.activeSubMode.PadReleased(x, y)
	}

}

func (a *BitwigClipMode) TrackStop(track uint8) {
	if track > 13 {
		return
	}
	//	fmt.Printf("stopping track %v\n", track)
	//a.clipSubmode.playtime.StopTrack(track + 1)
	a.unsetRunningTrack(track)

	a.Lock()
	active := a.clipSubmode.activeTrackClips[track]
	recording := a.clipSubmode.recordingClips[track]
	a.clipSubmode.activeTrackClips[track] = -1
	a.clipSubmode.recordingClips[track] = -1
	a.Unlock()
	if active >= 0 {
		a.clipSubmode.Set(uint8(active), track, colorFilledClip)
	}
	if recording >= 0 {
		a.clipSubmode.Set(uint8(active), track, colorFilledClip)
	}

	a.clipSubmode.highLightSelectedClip()
}

func (a *BitwigClipMode) triggerScene(scene uint8) {
	if scene > 6 {
		return
	}
	//	fmt.Printf("trigger scene %d\n", scene)
	//a.clipSubmode.playtime.TriggerScene(scene + 1)

	/*
		a.RLock()
		var foundFilled bool
		for track := uint8(0); track < sceneLaunchCol; track++ {
			if a.clipsFilled[track][scene] {
				foundFilled = true
			}
		}
		a.RUnlock()
	*/
	//	var _ = foundFilled

	//	if foundFilled {
	a.RLock()
	clipsFilled := a.clipSubmode.clipsFilled
	activeClips := a.clipSubmode.activeTrackClips
	recordingClips := a.clipSubmode.recordingClips
	a.RUnlock()

	for track := uint8(0); track < sceneLaunchCol; track++ {
		a.unsetRunningTrack(track)

		switch {
		case recordingClips[track] >= 0:
			a.Lock()
			rec := recordingClips[track]
			a.clipSubmode.recordingClips[track] = -1
			a.Unlock()
			//if rec != int8(scene) {
			a.clipSubmode.Set(uint8(rec), track, colorFilledClip)
			//}
		case activeClips[track] >= 0:
			a.Lock()
			act := activeClips[track]
			a.clipSubmode.activeTrackClips[track] = -1
			a.Unlock()
			//if activeClips[track] != int8(scene) {
			a.clipSubmode.Set(uint8(act), track, colorFilledClip)
			//}
		}

		if clipsFilled[track][scene] {
			a.Lock()
			a.clipSubmode.activeTrackClips[track] = int8(scene)
			a.Unlock()
			a.setRunningTrack(track)
			a.clipSubmode.Set(scene, track, colorActiveTrackClip)
		}

		//		time.Sleep(time.Millisecond * 8)
	}

	a.clipSubmode.highLightSelectedClip()
	//	}

	return

}

func (a *BitwigClipMode) StopAllClips() {
	//a.clipSubmode.playtime.StopAllClips()
	a.Lock()
	activeClips := a.clipSubmode.activeTrackClips
	recordingClips := a.clipSubmode.recordingClips
	a.Unlock()
	//	a.layout.BlankActiveTrackClips()
	for y := uint8(0); y < sceneLaunchCol; y++ {
		a.unsetRunningTrack(y)
		if recordingClips[y] >= 0 {
			a.clipSubmode.Set(uint8(recordingClips[y]), y, colorFilledClip)
		}
		if activeClips[y] >= 0 {
			a.clipSubmode.Set(uint8(activeClips[y]), y, colorFilledClip)
		}
	}
	a.clipSubmode.resetActiveAndRecording()

}

func (c *BitwigClipMode) controlPressed(x uint8) {

	switch x {
	case ctlRec:
		c.Set(ctlRec, clipControlCol, activeControlColor(ctlRec))
	case ctlUndo:
		bitwig.Undo.WriteTo(c)
		c.Set(ctlUndo, clipControlCol, activeControlColor(ctlUndo))
	case ctlDel:
		c.Set(ctlDel, clipControlCol, activeControlColor(ctlDel))
	case ctlClipStart:
		c.Lock()
		c.clipCutterMode = 1
		c.Unlock()
		c.Set(ctlRec, clipControlCol, MAGENTA)
		c.Set(ctlTap, clipControlCol, PINK)
		c.Set(groupStopRow, sceneLaunchCol, PINK)
	case ctlClipLength:
		c.Lock()
		c.clipCutterMode = 2
		c.Unlock()
		c.Set(ctlRec, clipControlCol, MAGENTA)
		c.Set(ctlTap, clipControlCol, PINK)
		c.Set(groupStopRow, sceneLaunchCol, PINK)
	//case ctlAutolatch:
	case ctlOverDub:
		c.doSwitchOverdub()
	case ctlClick:
		c.doClick()
	case ctlTap:
		c.RLock()
		clipCutterMode := c.clipCutterMode
		c.RUnlock()

		switch clipCutterMode {
		case 0:
			bitwig.TempoTap.WriteTo(c)
			//c.action.TempoTap()
			c.Set(ctlTap, clipControlCol, activeControlColor(ctlTap))
		case 1:
			//c.action.StopForSure()
			bitwig.Stop.WriteTo(c)
			time.Sleep(time.Millisecond * 10)
			//bitwig.TrackClipSelect.Set().
			//c.clipSubmode.playtime.SelectClipStartIncrement()
			c.Set(ctlTap, clipControlCol, RED)
		case 2:
			//c.action.StopForSure()
			bitwig.Stop.WriteTo(c)
			time.Sleep(time.Millisecond * 10)
			//c.clipSubmode.playtime.SelectClipLengthIncrement()
			c.Set(ctlTap, clipControlCol, RED)
		}
	}
}

func (a *BitwigClipMode) doPlay() {
	bitwig.Play.WriteTo(a)
	//reaper.ACTION.Set(40044).WriteTo(a.tr)
}

func (a *BitwigClipMode) doSwitchOverdub() {
	_, _, has := a.clipSubmode.getSelectedClip()
	_ = has

	if has {
		bitwig.Overdub.WriteTo(a)
		//	a.clipSubmode.playtime.MIDIOverdubClip()
	}

}

func (a *BitwigClipMode) doClick() {
	//reaper.METRONOME.WriteTo(a.tr)
	bitwig.Click.WriteTo(a)
}

func (c *BitwigClipMode) controlReleased(x uint8) {

	switch x {
	case ctlClipStart, ctlClipLength:
		c.Lock()
		c.clipCutterMode = 0
		c.Unlock()
		c.Set(ctlRec, clipControlCol, passiveControlColor(ctlRec))
		c.Set(ctlTap, clipControlCol, passiveControlColor(ctlTap))
		c.Set(groupStopRow, sceneLaunchCol, ORANGE)
	case ctlTap:
		c.RLock()
		clipCutterMode := c.clipCutterMode
		c.RUnlock()
		if clipCutterMode == 0 {
			c.Set(ctlTap, clipControlCol, passiveControlColor(ctlTap))
		} else {
			c.Set(ctlTap, clipControlCol, OFF)
		}
	case ctlUndo:
		c.Set(ctlUndo, clipControlCol, passiveControlColor(ctlUndo))
	case ctlDel: // delete selected
		c.clipSubmode.fillOrDeleteSelectedClip()
		c.Set(ctlDel, clipControlCol, passiveControlColor(ctlDel))
	case ctlRec: // trigger button
		c.Lock()
		clipCutterMode := c.clipCutterMode
		c.clipCutterMode = 0
		c.Unlock()
		switch clipCutterMode {
		case 0:
			c.clipSubmode.triggerSelectedClip()
		case 1:
			sel_x, sel_y, has := c.clipSubmode.getSelectedClip()
			fmt.Printf("selected: %v/%v has: %v\n", sel_x, sel_y, has)

			if !has {
				c.clipSubmode.selectClip(0, 0)
			}

			c.clipSubmode.RLock()
			rec := c.clipSubmode.recordingClips[sel_y]
			fmt.Printf("recording clips: %#v\n", c.clipSubmode.recordingClips)
			c.clipSubmode.RUnlock()

			c.clipSubmode.triggerSelectedClip()
			if rec == int8(sel_x) && sel_y < sceneLaunchCol-1 {
				fmt.Printf("was recording and is stopped, going right\n")
				time.Sleep(time.Millisecond * 10)
				c.clipSubmode.selectClip(sel_x, sel_y+1)
				//				time.Sleep(time.Millisecond * 200)
				//				c.clipSubmode.triggerSelectedClip()
			} else {
				fmt.Printf("recording has been started\n")
			}

		case 2:
			sel_x, sel_y, has := c.clipSubmode.getSelectedClip()
			fmt.Printf("selected: %v/%v has: %v\n", sel_x, sel_y, has)

			if !has {
				c.clipSubmode.selectClip(0, 0)
			}

			c.clipSubmode.RLock()
			rec := c.clipSubmode.recordingClips[sel_y]
			fmt.Printf("recording clips: %#v\n", c.clipSubmode.recordingClips)
			c.clipSubmode.RUnlock()

			c.clipSubmode.triggerSelectedClip()
			if rec == int8(sel_x) && sel_x < groupStopRow-1 {
				fmt.Printf("was recording and is stopped, going right\n")
				time.Sleep(time.Millisecond * 10)
				c.clipSubmode.selectClip(sel_x+1, sel_y)
				//				time.Sleep(time.Millisecond * 200)
				//				c.clipSubmode.triggerSelectedClip()
			} else {
				fmt.Printf("recording has been started\n")
			}
		}
		c.Set(ctlRec, clipControlCol, passiveControlColor(ctlRec))
	default:
		// nothing
	}
}

func (c *BitwigClipMode) String() string {
	return "bitwig clip mode"
}

func (t *BitwigClipMode) Set(x, y uint8, color linnstrument.Color) {
	t.Triggerer.SetColor(x, y+1, color)
}

const (
	ctlRec       uint8 = 0
	ctlUndo      uint8 = 1
	ctlDel       uint8 = 2
	ctlClipStart uint8 = 3
	//	ctlPlay      uint8 = 1
	//	ctlAutolatch uint8 = 3
	ctlOverDub    uint8 = 4
	ctlClipLength uint8 = 5
	ctlClick      uint8 = 6
	ctlTap        uint8 = 7
)

func activeControlColor(row uint8) linnstrument.Color {
	switch row {
	case ctlRec:
		return OFF
		//	case ctlPlay:
		//		return GREEN
	case ctlUndo:
		return GREEN
	case ctlDel:
		return OFF
		//	case ctlAutolatch:
		//		return MAGENTA
	case ctlOverDub:
		return MAGENTA
	case ctlClick:
		return CYAN
	case ctlTap:
		return RED
	default:
		return OFF
	}
}

func passiveControlColor(row uint8) linnstrument.Color {
	switch row {
	case ctlRec:
		return RED
		//	case ctlPlay:
		//		return GREEN
	case ctlUndo:
		return ORANGE
	case ctlDel:
		return WHITE
		//	case ctlAutolatch:
		//		return PINK
	case ctlOverDub:
		return PINK
	case ctlClick:
		return BLUE
	case ctlTap:
		return LIME
	default:
		return OFF
	}
}

func (c *BitwigClipMode) paintToolbar() {
	c.RLock()
	metro := c.metronomeOn

	c.RUnlock()
	for x := uint8(0); x < 8; x++ {
		color := passiveControlColor(x)
		switch x {
		case ctlClick:
			if metro {
				color = activeControlColor(x)
			}
		//case ctlOverDub:

		default:
		}
		c.Set(x, 15, color)
	}
	/*
		for _, cb := range clipControlButtons {
			c.Set(cb.Row(), 15, cb.Color())
		}
	*/
}

func (c *BitwigClipMode) TransposePressed() linnstrument.Color {
	if c.activeSubMode == c.transposeSubmode {
		c.switchSubMode(c.playSubmode)
		return OFF
	}

	c.switchSubMode(c.transposeSubmode)
	return PINK
}
