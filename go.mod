module gitlab.com/golinnstrument/linnwig

go 1.14

require (
	gitlab.com/golinnstrument/linnstrument v1.0.1
	gitlab.com/gomidi/midi v1.23.7
	gitlab.com/gomidi/midicatdrv v0.3.7 // indirect
	gitlab.com/gomidi/rtmididrv v0.13.0
	gitlab.com/goosc/bitwig v0.0.8 // indirect
	gitlab.com/goosc/osc v0.2.0
	gitlab.com/metakeule/config v1.21.0
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
)
